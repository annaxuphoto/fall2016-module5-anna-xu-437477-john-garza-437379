Creative Portion

We implemented the following non-required features in Module 5:

Users can log out.
Today's date is highlighted.
The calendar alerts the users of how many events they have that day (today).
Users can share their calendars to Facebook.
Users can remove all events from any given day instead of having to remove them one by one.

server link: http://ec2-52-207-237-51.compute-1.amazonaws.com/~annaxu/calendar.html