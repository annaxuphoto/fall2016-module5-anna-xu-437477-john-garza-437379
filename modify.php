<?php

require 'mysqlLogin.php';
ini_set("session.cookie_httponly", 1);
session_start();

if ($_SESSION['token'] != $_POST['token']) {
    exit;
}

$stmt = $mysqli->prepare('UPDATE events SET title=?, time=? WHERE pk_id=?');
if (!$stmt) {
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
}
$stmt->bind_param('ssi', $_POST['title'], $_POST['time'], $_POST['id']);
$stmt->execute();

?>