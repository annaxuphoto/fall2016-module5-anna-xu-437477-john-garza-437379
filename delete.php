<?php

require 'mysqlLogin.php';
ini_set("session.cookie_httponly", 1);
session_start();

if ($_SESSION['token'] != $_POST['token']) {
    exit;
}

$stmt = $mysqli->prepare('DELETE FROM events WHERE pk_id=? AND user=?');
if (!$stmt) {
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
}
$stmt->bind_param("is", $_POST['id'], $_SESSION['user']);
$stmt->execute();
exit;

?>