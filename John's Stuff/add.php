<?php

require 'mysqlLogin.php';
ini_set("session.cookie_httponly", 1);

session_start();

$stmt = $mysqli->prepare('INSERT INTO events (title, day, month, year, time, user) VALUES (?, ?, ?, ?, ?, ?)');
if (!$stmt) {
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
}
$stmt->bind_param("siiiss", $_POST['title'], $_POST['day'], $_POST['month'], $_POST['year'], $_POST['time'], $_SESSION['user']);
$stmt->execute();
echo "done";
exit;

?>
