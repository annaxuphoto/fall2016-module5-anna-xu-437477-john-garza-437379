<?php

require 'mysqlLogin.php';

$stmt = $mysqli->prepare('SELECT title, day, month, year, time FROM events WHERE pk_id=?');
$stmt->bind_param("i", $_POST['id']);
$stmt->execute();
$stmt->bind_result($title, $day, $month, $year, $time);
while ($stmt->fetch()) {
    if (preg_match("/[\w\s]+/", $title) && preg_match("/\d{1,2}:\d{2}/", $time)) {
        $array = array("title" => $title, "day" => $day, "month" => $month, "year" => $year, "time" => $time);
    }
}
echo json_encode($array);
exit;

?>