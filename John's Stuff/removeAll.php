<?php

require 'mysqlLogin.php';
ini_set("session.cookie_httponly", 1);
session_start();

if ($_SESSION['token'] != $_POST['token']) {
    exit;
}
$stmt = $mysqli->prepare('DELETE FROM events WHERE day=? AND month=? AND year=?');
$stmt->bind_param("iii", $_POST['day'], $_POST['month'], $_POST['year']);
$stmt->execute();

?>