<?php
//from wiki example

require "mysqlLogin.php";

header("Content-Type: application/json");

$user = $_POST['username'];
$pw = $_POST['password'];
$set = false;

//Retrieve users and encrypted pws from database
$stmt = $mysqli->prepare("SELECT name, pw FROM users");
if (!$stmt) {
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
}
$stmt->execute();
$stmt->bind_result($dataUser, $dataPW);

//Check each user/pw pair with input user and recently encrypted/salted pw
while ($stmt->fetch()) {
    //Login if match is found and create CSRF token
    if ($user == $dataUser && crypt($pw, $dataPW) == $dataPW) {
        ini_set("session.cookie_httponly", 1);

        session_start();
        $_SESSION['user'] = $user;
        $token = substr(md5(rand()), 0, 10);
        $_SESSION['token'] = $token;
        $set = true;
    }
}

if ($set) {
    if (preg_match("/\w+/", $user)) {
        echo json_encode(array(
            "success" => true,
            "username" => "$user",
            "token" => $token));
    }
    exit;
} else {
    echo json_encode(array(
        "success" => false,
        "message" => "Incorrect Username or Password"
    ));
    exit;
}
?>