<?php

require 'mysqlLogin.php';

$stmt = $mysqli->prepare('INSERT INTO users (name, pw) VALUES (?, ?)');
$user = $_POST['newUser'];

if (!preg_match(' /^[\w_\-]+$/', $user)) {
    exit;
}

if (!preg_match(' /^[\w_\-]+$/', $_POST['newPass'])) {
    exit;
}

if ($_POST['newPass'] == $_POST['confirmPass']) {
    $encryptedPW = crypt($_POST['newPass']);
    $stmt->bind_param('ss', $user, $encryptedPW);
    $stmt->execute();
}
else {
    echo 'alert("Passwords do not match.")';
}

?>