<?php

require "mysqlLogin.php";
ini_set("session.cookie_httponly", 1);

session_start();

$stmt = $mysqli->prepare("SELECT pk_id, title, day, month, year, time FROM events WHERE user=? AND month=? AND year=?");
if (!$stmt) {
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
}
$stmt->bind_param("sss", $_SESSION['user'], $_POST['month'], $_POST['year']);
$stmt->execute();
$stmt->bind_result($id, $title, $day, $month, $year, $time);

$events = array();
$dummy = 0;

while ($stmt->fetch()) {
    if (preg_match("/[\w\s]+/", $title) && preg_match("/\d{1,2}:\d{2}/", $time)) {
        $events[$dummy] = array("id" => $id, "title" => $title, "day" => $day, "month" => $month, "year" => $year, "time" => $time);
        $dummy = $dummy + 1;
    }
}
echo json_encode($events);
exit;

?>