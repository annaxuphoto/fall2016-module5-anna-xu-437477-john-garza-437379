
// Get the modal
var modal = document.getElementById("userInfo");
//modal.style.display = "none";
        
// Get the button that opens the modal
//var btn = document.getElementById("login");
        
// Get the <span> element that closes the modal
var span = modal.getElementsByClassName("close")[0];
// When the user clicks on the button, open the modal 
//btn.addEventListener("click", function() {
//    modal.style.display = "block";
//}, false);
        
// When the user clicks on <span> (x), close the modal
span.addEventListener("click", function() {
    modal.style.display = "none";
}, false);
      
// When the user clicks anywhere outside of the modal, close it
window.addEventListener("click", function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}, false);

        (function () {
            Date.prototype.deltaDays = function (c) {
                return new Date(this.getFullYear(), this.getMonth(), this.getDate() + c);
                };
            Date.prototype.getSunday = function () {
                return this.deltaDays(-1 * this.getDay());
            };
        })();

        function Week(c) {
            this.sunday = c.getSunday();
            this.nextWeek = function () {
                return new Week(this.sunday.deltaDays(7));
            };
            this.prevWeek = function () {
                return new Week(this.sunday.deltaDays(-7));
            };
            this.contains = function (b) {
                return this.sunday.valueOf() === b.getSunday().valueOf();
            };
            this.getDates = function () {
                for (var b = [], a = 0; 7 > a; a++) {
                    b.push(this.sunday.deltaDays(a));
                }
                return b;
            };
        }

        function Month(c, b) {
            this.year = c;
            this.month = b;
            this.nextMonth = function () {
                return new Month(c + Math.floor((b + 1) / 12), (b + 1) % 12);
            };
            this.prevMonth = function () {
                return new Month(c + Math.floor((b - 1) / 12), (b + 11) % 12);
            };
            this.getDateObject = function (a) {
                return new Date(this.year, this.month, a);
            };
            this.getWeeks = function () {
                var a = this.getDateObject(1), b = this.nextMonth().getDateObject(0), c = [], a = new Week(a);
                for (c.push(a); !a.contains(b);) {
                    a = a.nextWeek(), c.push(a);
                }
                return c;
            };
        }

        var initialDate = new Date();
        var currentMonth = new Month(initialDate.getFullYear(), initialDate.getMonth());
        var loggedIn = false;
        var token = 0;
        
        function monthToName(month) {
            if(month == 1) {
                return "January";
            }
            if(month == 2) {
                return "February";
            }
            if(month == 3) {
                return "March";
            }
            if(month == 4) {
                return "April";
            }
            if(month == 5) {
                return "May";
            }
            if(month == 6) {
                return "June";
            }
            if(month == 7) {
                return "July";
            }
            if(month == 8) {
                return "August";
            }
            if(month == 9) {
                return "September";
            }
            if(month == 10) {
                return "October";
            }
            if(month == 11) {
                return "November";
            }
            if(month == 12) {
                return "December";
            }
        }

        //initiates page
        function initiate() {
            document.getElementById("login").addEventListener("click", attemptLogin);
            document.getElementById("createNew").addEventListener("click", createUser);
            //document.getElementById("next").addEventListener("click", next);
            //document.getElementById("previous").addEventListener("click", previous);
            //document.getElementById("month").innerHTML = (currentMonth.month + 1) + ", " + currentMonth.year;
            document.getElementById("head").getElementsByClassName("unorderedList")[0].getElementsByClassName("prev")[0].addEventListener("click", previous, false);
            document.getElementById("head").getElementsByClassName("unorderedList")[0].getElementsByClassName("next")[0].addEventListener("click", next, false);
            document.getElementById("head").getElementsByClassName("unorderedList")[0].getElementsByClassName("month")[0].innerHTML = (monthToName(currentMonth.month + 1)) + "<br>" + currentMonth.year;
            addButtons();
        }

        //function to remove all events on day from database
        function removeAll(day) {
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open("POST", "removeAll.php");
            xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            var encoded = "day=" + day.getDate() + "&month=" + day.getMonth() + "&year=" + day.getFullYear() + "&token=" + token;
            xmlHttp.addEventListener("load", getEvents, false);
            xmlHttp.send(encoded);
        }

        //creates input modals for adding an event
        function addModal(date) {
            var title = window.prompt("Title: ");
            if (/[\w\s]+/.test(title) === true) {
                var time = window.prompt("Time: ");
                if (/\d{1,2}:\d{2}/.test(time) === true) {
                    var xmlHttp = new XMLHttpRequest();
                    xmlHttp.open("POST", "add.php");
                    xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    var encoded = "title=" + title + "&time=" + time + "&day=" + (date.getDate()) + "&month=" + date.getMonth() + "&year=" + date.getFullYear();
                    xmlHttp.addEventListener("load", getEvents, false);
                    xmlHttp.send(encoded);
                    //var split = time.split(":");
                    //var event = new Date(date.getFullYear(), date.getMonth(), date.getDate(), split[0], split[1], 0);
                    //alert("Current Time: " + initialDate.getTime() + ", Event Time: " + event.getTime());
                }
                else {
                    alert("Invalid Time");
                }
            }
            else {
                alert("Invalid Title");
            }
        }

        //move to next month
        function next() {
            currentMonth = currentMonth.nextMonth();
            getEvents();
            document.getElementById("head").getElementsByClassName("unorderedList")[0].getElementsByClassName("month")[0].innerHTML = (monthToName(currentMonth.month + 1)) + "<br>" + currentMonth.year;
        }

        //moves to previous month
        function previous() {
            currentMonth = currentMonth.prevMonth();
            getEvents();
            document.getElementById("head").getElementsByClassName("unorderedList")[0].getElementsByClassName("month")[0].innerHTML = (monthToName(currentMonth.month + 1)) + "<br>" + currentMonth.year;
        }

        //run when name/pw is submitted for login
        function attemptLogin() {
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open("POST", 'ajaxLogin.php', true);
            xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            var username = document.getElementById("username").value;
            var password = document.getElementById("password").value;
            var encoded = "username=" + username + "&password=" + password;
            xmlHttp.addEventListener("load", updateUser, false);
            xmlHttp.send(encoded);
        }

        //updates current user section and events if login was successful
        function updateUser() {
            var jsonData = JSON.parse(event.target.responseText);
            var logoutButton = document.getElementById("logout");

            if (jsonData.success) {
                loggedIn = true;
                token = jsonData.token;

                //remove the section containing
                //TODO replace with jquery show/hide http://stackoverflow.com/questions/6684545/unhide-hide-a-class
                var doneNode = document.getElementById("userInfo");
                //doneNode.parentNode.removeChild(doneNode);
                //doneNode.style.display = "none";
                $(doneNode).hide();
                getEvents();
                
                //logoutButton.innerHTML = "Logout";
                //$(logoutButton).click(logout());
                logoutButton.addEventListener("click", logout, false);
                $(logoutButton).show();
            }
        }
        
        function logout() {
            loggedIn = false;
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open("POST", "logout.php", true);
            xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xmlHttp.send();
            clear();
            addButtons();
            var userInfo = document.getElementById("userInfo");
            $(userInfo).show();
            var logoutButton = document.getElementById("logout");
            $(logoutButton).hide();
        }

        //creates user
        function createUser() {
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open("POST", "newUser.php", true);
            xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            var username = document.getElementById("newUser").value;
            var password = document.getElementById("newPass").value;
            var confirmPass = document.getElementById("confirmPass").value;
            var encoded = "newUser=" + username + "&newPass=" + password + "&confirmPass=" + confirmPass;
            xmlHttp.send(encoded);
        }

        //add buttons to cells
        function addButtons() {
            var weeks = currentMonth.getWeeks();
            var j = 0;
            for (var w in weeks) {
                var days = weeks[w].getDates();
                for (var d in days) {
                    j++;
                    if (days[d].getMonth() == currentMonth.month) {
                        (function (j, loggedIn) {
                            //adds date
                            if(days[d].getDate() == initialDate.getDate()) {
                            var element = document.getElementById(String(j));
                            element.innerHTML += '<br><span class="active">' + String(days[d].getDate()) + '</span>';
                            }
                            else {
                                document.getElementById(String(j)).appendChild(document.createElement("br"));
                            var text = document.createTextNode(String(days[d].getDate()));
                            document.getElementById(String(j)).appendChild(text);
                            }
                            if (loggedIn) {
                                //creates button to add events
                                var currButton = document.createElement("button");
                                var currDay = days[d];
                                currButton.innerHTML = "Add";
                                currButton.addEventListener("click", function () {
                                    addModal(currDay);
                                }, true);
                                document.getElementById(String(j)).appendChild(currButton);
                                //creates button to remove all events
                                var removeButton = document.createElement("button");
                                removeButton.innerHTML = "Remove All";
                                removeButton.addEventListener("click", function () {
                                    removeAll(currDay);
                                }, true);
                                document.getElementById(String(j)).appendChild(removeButton);
                            }
                        }(j, loggedIn));
                    }
                }
            }
        }

        //retrieves events for user from database
        function getEvents() {
            clear();
            addButtons();
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open("POST", "events.php");
            xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xmlHttp.addEventListener("load", eventsCallback, false);
            
            var encoded = "month=" + String(currentMonth.month) + "&year=" + String(currentMonth.year);
            xmlHttp.send(encoded);
        }

        //populates calendar with events data
        function eventsCallback() {
            var jsonData = JSON.parse(event.target.responseText);
            //populate calendar here, some code taken from wiki
            var delta = currentMonth.getDateObject(1).getDay();
            var weeks = currentMonth.getWeeks();
            var count = 0;
            var today = new Date();
            if (loggedIn) {
                for (var w in weeks) {
                    var days = weeks[w].getDates();
                    for (var day in days) {
                        if (days[day].getMonth() == currentMonth.month) {
                            for (i = 0; i < jsonData.length; i++) {
                                if (currentMonth.getDateObject(jsonData[i].day).getTime() == days[day].getTime()) {
                                    if(days[day].getDate() == today.getDate()) {
                                        count++;
                                    }
                                    var added = days[day].getDate() + delta;
                                    var linebreak = document.createElement("br");
                                    document.getElementById(String(added)).appendChild(linebreak);
                                    //show title and time
                                    var text = jsonData[i].time + " " + jsonData[i].title;
                                    var node = document.createTextNode(text);
                                    document.getElementById(String(added)).appendChild(node);
                                    //create delete button
                                    var deleteNode = document.createElement("button");
                                    deleteNode.innerHTML = "Delete";
                                    (function () {
                                        var id = jsonData[i].id;
                                        deleteNode.addEventListener("click", function () {
                                            deleteEvent(id);
                                        }, false);
                                        document.getElementById(String(added)).appendChild(deleteNode);
                                        //create modify button
                                        var modifyButton = document.createElement("button");
                                        modifyButton.innerHTML = "Modify";
                                        modifyButton.addEventListener("click", function () {
                                            modifyEvent(id);
                                        }, false);
                                        document.getElementById(String(added)).appendChild(modifyButton);
                                    }());
                                }
                            }
                        }
                    }
                }
            }
            if(count > 0) {
                alert("You have " + count + " events today!");
            }
        }
        //clears table of events
        function clear() {
            for (i = 1; i < 43; i++) {
                var node = document.getElementById(String(i));
                while (node.hasChildNodes()) {
                    node.removeChild(node.lastChild);
                }
            }
        }

        //modifies event
        function modifyEvent(id) {
            //get changed data for event
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open("POST", "update.php");
            xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            //create url with all changed event data and send data while adding listener for reload
            var encoded = "id=" + id;
            xmlHttp.addEventListener("load", function () {
                modifyModal(id);
            }, false);
            xmlHttp.send(encoded);
        }

        //popup to input information for modifying event
        function modifyModal(id) {
            var jsonData = JSON.parse(event.target.responseText);
            var title = window.prompt("Change title:", jsonData.title);
            if (/[\w\s]+/.test(title) === true) {
                var time = window.prompt("Change time:", jsonData.time);
                if (/\d{1,2}:\d{2}/.test(time) === true) {
                    var xmlHttp = new XMLHttpRequest();
                    xmlHttp.open("POST", "modify.php");
                    xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xmlHttp.addEventListener("load", getEvents, false);
                    var encoded = "id=" + id + "&title=" + title + "&time=" + time + "&token=" + token;
                    xmlHttp.send(encoded);
                }
                else {
                    alert("Invalid Time");
                }
            }
            else {
                alert("Invalid Title");
            }
        }

        //delete event from calendar
        function deleteEvent(id) {
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open("POST", "delete.php", true);
            xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            //create encoded url containing event id and send data while adding listener for reload
            var encoded = "id=" + String(id) + "&token=" + token;
            xmlHttp.addEventListener("load", getEvents, false);
            xmlHttp.send(encoded);
        }

        //wait for initial page structure to load then initialize event listeners
        document.addEventListener("DOMContentLoaded", initiate, false);
        